import json, requests, urllib
from urlparse import urlparse
import os
from operator import itemgetter
from collections import Counter

def callWooliesApi(resource):
	accessToken = os.environ['TOKEN']
	apiHost = os.environ['APIHOST'] 
	requestUrl = 'http://{}/api/resource/{}?token={}'.format(apiHost, resource, accessToken)
	try:
		response = requests.get(requestUrl)
		return json.loads(response.content)
	except requests.exceptions.RequestException as e:
		print e	


def callSort(itemList, sortKey, sortReverse=False):
	try:
		sortContent = sorted(itemList, key=itemgetter(sortKey), reverse=sortReverse)
		return sortContent
	except Exception as e:
		print 'oops!! something went wrong: {}'.format(e)
			

def lowToHigh():
	content = callWooliesApi('products')
	sortContent = callSort(content,'price')
	return json.dumps(sortContent)


def highToLow():
	content = callWooliesApi('products')
	sortContent = callSort(content, 'price', True)
	return json.dumps(sortContent)


def ascendingOrder():
	content = callWooliesApi('products')
	sortContent = callSort(content, 'name')
	return json.dumps(sortContent)


def descendingOrder():
	content = callWooliesApi('products')
	sortContent = callSort(content, 'name', True)	
	return json.dumps(sortContent)


def recommended():
	content = callWooliesApi('shopperHistory')

	tmpPopular = {}
	popularItem = []


	for customer in content:
		for product in customer['products']:
			tmpPopular[product['name']] = tmpPopular.get(product['name'], 0) + 1
    
    
	for k,v in tmpPopular.items():
		popularItem.append({ 'name': k, 'occurances': v})

	sortContent = callSort(popularItem, 'occurances', True)

	unsortedProduct = callWooliesApi('products')

	sortedProduct = [ p  for pr in sortContent for p in unsortedProduct if pr['name'] in p['name'] ]

	return json.dumps(sortedProduct)