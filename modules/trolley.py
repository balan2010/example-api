import json, requests, urllib
from urlparse import urlparse
import os
from operator import itemgetter
from collections import Counter
from sortMethod import callWooliesApi
from flask import Response

def callWooliesApi(resource, payload):
	accessToken = os.environ['TOKEN']
	apiHost = os.environ['APIHOST'] 
	#accessToken = 'test'
	requestUrl = 'http://{}/api/resource/{}?token={}'.format(apiHost, resource, accessToken)
	try:
		headers = {'content-type': 'application/json-patch+json', 'Accept': 'application/json'}
		response = requests.post(requestUrl, data=json.dumps(payload), headers=headers)	
		return response.text, response.status_code
	except requests.exceptions.RequestException as e:
		print e	


def calculator(payload):
	r = callWooliesApi('trolleyCalculator', payload)
	return r

