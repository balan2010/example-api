import json, requests, urllib
from urlparse import urlparse
import os
import sortMethod
from sortMethod import lowToHigh, highToLow, ascendingOrder, descendingOrder, recommended


def route(sortmethod):
	return sortMethod[sortmethod]()


sortMethod = {
	'Low': sortMethod.lowToHigh,
	'High': sortMethod.highToLow,
	'Ascending': sortMethod.ascendingOrder,
	'Descending': sortMethod.descendingOrder,
	'Recommended': sortMethod.recommended
}