FROM python:2.7.15-jessie

ENV TZ Australia/Sydney

COPY . /usr/share/example-api

WORKDIR /usr/share/example-api

RUN pip install -r requirements.txt

CMD ["python", "api.py"] 