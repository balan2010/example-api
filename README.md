### Hosting

Application runs in Google Cloud on Kubernetes. 

### Architecture

nginx sits infront of api and forwards traffic based on request url

nginx and api deployed in single pod so that nginx can forward traffic through localhost

API is written in python with Flask framework 

### Accessing

Api can be accessed via

http://35.189.34.252

### Following URL has been tested for Exercise - 2



```
http://35.189.34.252/api/answers

http://35.189.34.252/sort?sortOption=High

http://35.189.34.252/sort?sortOption=High

http://35.189.34.252/sort?sortOption=Low

http://35.189.34.252/sort?sortOption=Ascending

http://35.189.34.252/sort?sortOption=Descending

http://35.189.34.252/sort?sortOption=Recommended ## Based number of customers bought a product
```

### Not Sure what to do with Exercise - 3

Below is the example postman config thats been tried

```
{
	"info": {
		"_postman_id": "f2675964-b881-4a9f-b144-35fb73848988",
		"name": "wooliesx",
		"schema": "https://schema.getpostman.com/json/collection/v2.1.0/collection.json"
	},
	"item": [
		{
			"name": "http://35.189.34.252/trolleyCalculator",
			"request": {
				"method": "POST",
				"header": [
					{
						"key": "Accept",
						"value": "application/json"
					},
					{
						"key": "Content-Type",
						"value": "application/json"
					}
				],
				"body": {
					"mode": "raw",
					"raw": "{ \n   \"products\": [ \n     { \n       \"name\": \"string\",  \n       \"price\": 0 \n     } \n   ],  \n   \"specials\": [  \n     { \n       \"quantities\": [  \n         { \n           \"name\": \"string\",  \n           \"quantity\": 0 \n         } \n       ], \n       \"total\": 0  \n     } \n   ],  \n   \"quantities\": [  \n     {  \n       \"name\": \"string\",  \n       \"quantity\": 0 \n     } \n   ] \n }"
				},
				"url": {
					"raw": "http://35.189.34.252/trolleyCalculator",
					"protocol": "http",
					"host": [
						"35",
						"189",
						"34",
						"252"
					],
					"path": [
						"trolleyCalculator"
					]
				},
				"description": "api\n\nhttp://35.189.34.252/trolleyCalculator"
			},
			"response": []
		}
	]
}
```




