import logging
import os
from flask import Flask, jsonify, json, url_for, redirect, Response
from flask import request
from modules import router, trolley


app = Flask(__name__)
logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

@app.route('/user', methods=['GET'])
def user():
	return json.dumps({"name": "Umabalan Nagarajan", "token" : "c75fe2c0-ed77-4251-a92c-1af7fd446036"})

@app.route('/sort', methods=['GET'])
def sort():
	sortOpt = request.args.get('sortOption')
	return router.route(sortOpt)

@app.route('/trolleyCalculator', methods=['GET','POST'])
def trolleyCalculator():
	content = request.json
	r = trolley.calculator(content)
	return r

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
